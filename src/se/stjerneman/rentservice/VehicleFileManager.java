package se.stjerneman.rentservice;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

import se.stjerneman.rentservice.items.Vehicle;

/**
 * A class that handles vehicles IO.
 * 
 * @author Emil Stjerneman
 * 
 */
public class VehicleFileManager extends FileManager {

    /**
     * VehicleFileManager constructor.
     * 
     * @param fileName
     *            the file name.
     */
    public VehicleFileManager(String fileName) {
        super(fileName, "");
    }

    /**
     * VehicleFileManager constructor.
     * 
     * @param fileName
     *            the file name.
     * @param filePath
     *            the file path.
     */
    public VehicleFileManager(String fileName, String filePath) {
        super(fileName, filePath);
    }

    /**
     * Loads this file and return the content.
     * 
     * @return a HashMap with vehicles.
     */
    @SuppressWarnings("unchecked")
    public HashMap<String, Vehicle> load() {
        HashMap<String, Vehicle> list = null;

        ObjectInputStream ois = null;

        try {
            FileInputStream fis = new FileInputStream(this.getFile());
            ois = new ObjectInputStream(fis);

            // Type cast the object from the stream to a HashMap.
            list = (HashMap<String, Vehicle>) ois.readObject();
        }
        catch (EOFException e) {
            // End of file.
            // Feels stupid to me that EOF would throw an exception but I guess
            // I'll have to live with that.
        }
        catch (FileNotFoundException e) {
            System.err.printf("The file \"%s\" couldn't be found.%n",
                    this.getFile().getPath());
            Router.stopApplication(e);
        }
        catch (IOException e) {
            System.out.println("IOException WTF?");
            Router.stopApplication(e);
        }
        catch (ClassNotFoundException e) {
            Router.stopApplication(e);
        }
        finally {
            // Close the stream.
            if (ois != null) {
                try {
                    ois.close();
                }
                catch (IOException e) {
                    System.err.println("Couldn't close the ObjectInputStream.");
                }
            }
        }

        if (list == null) {
            list = new HashMap<String, Vehicle>();
        }

        return list;
    }

    /**
     * Saves a HashMap to the file.
     * 
     * @param vehicleList
     *            a HashMap of vehicles.
     */
    public void save(HashMap<String, Vehicle> vehicleList) {
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(new FileOutputStream(this.getFile()));
            oos.writeObject(vehicleList);
        }
        catch (IOException e) {
            Router.stopApplication(e);
        }
        finally {
            try {
                oos.close();
            }
            catch (IOException e) {
                System.err.println("Couldn't close the ObjectOutputStream.");
            }
        }
    }
}
