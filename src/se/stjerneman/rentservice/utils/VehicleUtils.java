package se.stjerneman.rentservice.utils;

import java.util.Map;

import se.stjerneman.rentservice.ListVehicles;
import se.stjerneman.rentservice.items.Vehicle;

/**
 * Vehicle utils.
 * 
 * @author Emil Stjerneman
 */
// TODO: Move this to Vehcile.java instead.
public class VehicleUtils {

    /**
     * Tests whether the registration number is valid. Validate format is [ABC
     * 123].
     * 
     * @param registrationNumber
     *            a registration number to validate.
     * @return true when the registration number is valid, otherwise false.
     */
    public static boolean validateRegistrationnumber(String registrationNumber) {
        return registrationNumber.matches("^[A-Z]{3} [0-9]{3}$");
    }

    /**
     * Gets a vehicle by a registration number.
     * 
     * @param registrationNumber
     *            a registration number
     * @return a vehicle with the registration number if its exists, otherwise
     *         null.
     */
    public static Vehicle getVehcile(String registrationNumber) {
        // Get the list if existing vehicles.
        Map<String, Vehicle> list = ListVehicles.getList();

        // Get the vehicle with the registration number, if its exists.
        if (list.get(registrationNumber) != null) {
            return list.get(registrationNumber);
        }

        return null;
    }
}
