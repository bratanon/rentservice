package se.stjerneman.rentservice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import se.stjerneman.rentservice.items.Car;
import se.stjerneman.rentservice.items.Trailer;
import se.stjerneman.rentservice.items.Truck;
import se.stjerneman.rentservice.items.Vehicle;
import se.stjerneman.rentservice.utils.VehicleUtils;

/**
 * This class will handle the application routes.
 * 
 * @author Emil Stjerneman
 * 
 */
public class Router {

    /**
     * A buffered reader instance to use when getting user input.
     */
    public static BufferedReader bufferedReader = new BufferedReader(
            new InputStreamReader(System.in));

    /**
     * Router constructor.
     */
    public Router() {
        this.init();
    }

    /**
     * Initializing the router.
     */
    private void init() {
        System.out.println("#######################################");
        System.out.println("##      Welcome to Rent service      ##");
        System.out.println("#######################################");
        System.out.printf("%n%n");

        // Print the main menu.
        Router.printMainMenu();

        while (true) {
            // Get the action.
            String action = Router.getMenuOption();

            // Switch between actions.
            switch (action) {
                case "add":
                    Router.addVehicle();
                    break;
                case "edit":
                    Router.editVehicle();
                    break;
                case "delete":
                    Router.deleteVehicle();
                    break;
                case "list":
                    Router.listVehicles();
                    break;
                case "list all":
                    Logger.info("Listed  all.");
                    ListVehicles.listAll();
                    break;
                case "view":
                    Router.viewVehicle();
                    break;
                case "rent":
                    Router.rent();
                    break;
                case "return":
                    Router.rentReturn();
                    break;
                case "view log":
                    Logger.viewLog();
                    break;
                case "menu":
                    // Print the main menu.
                    Router.printMainMenu();
                    break;
                case "exit":
                    Router.exit();
            }

            System.out.println("Print \"menu\" to see the main menu again.");
        }
    }

    /**
     * Prints the main menu.
     */
    private static void printMainMenu() {
        System.out.println("------------------------------------");
        System.out.println("Main menu");
        System.out.println("------------------------------------");
        System.out.println("TODO: (rent) - Rent out a vehicle");
        System.out.println("TODO: (return) - Return a vehicle");
        System.out.println();
        System.out.println("(list) - List vehicles.");
        System.out.println();
        System.out.println("------------------------------------");
        System.out.println("Administration menu");
        System.out.println("------------------------------------");
        System.out.println("(add) - Add a vehcile");
        System.out.println("(edit) - Edit a vehicle.");
        System.out.println("(delete) - Delete a vehicle.");
        System.out.println();
        System.out.println("(exit) - Exit!");
        System.out.println();
        System.out.println("- - - - - - - - - - - - - - - - - - ");
    }

    /**
     * Gets the menu option.
     * 
     * @return an action to perform.
     */
    private static String getMenuOption() {
        System.out.print("Please choose a valid option: ");

        String action = null;

        try {
            // Read console response.
            action = Router.bufferedReader.readLine().trim();
        }
        catch (IOException e) {
            Logger.error("IOException in Router.getMenuOption().");
            Router.stopApplication(e);
        }

        // A list of valid menu options.
        List<String> validOptions = Router.validMainMenuOptions();

        // Validate the action.
        if (!validOptions.contains(action.split(" ")[0])) {
            System.out.println("Couldn't find the option.");
            // Call this method recursively to get an action.
            return Router.getMenuOption();
        }

        return action;
    }

    /**
     * Returns valid options for the main menu.
     * 
     * @return a list of valid options for the main menu.
     */
    private static List<String> validMainMenuOptions() {
        String[] options = {
                "exit",
                "menu",
                "add",
                "edit",
                "delete",
                "list",
                "list all",
                "view",
                "rent",
                "return",
                "view log"
        };
        return new ArrayList<String>(Arrays.asList(options));
    }

    /**
     * Gets the registration number from the user.
     * 
     * @return a registration number.
     */
    private static String getRegistrationNumber() {
        String registrationNumber = null;

        try {
            while (true) {
                System.out.print("Registraiton number: ");
                registrationNumber = Router.bufferedReader.readLine().trim()
                        .toUpperCase();
                if (VehicleUtils.validateRegistrationnumber(registrationNumber)) {
                    break;
                }
                System.out.println("The format must be [ABC 123]");
            }
        }
        catch (IOException e) {
            Logger.error("IOException in Router.getRegistrationNumber().");
            Router.stopApplication(e);
        }

        return registrationNumber;
    }

    /**
     * Gets a type of vehicle from the user.
     * 
     * @param options
     *            valid types the user can choose from.
     * 
     * @return the type the user selected.
     */
    private static String getVehicleType(List<String> options) {
        String type = null;

        try {
            while (true) {
                System.out.printf("What type %s: ", options.toString());
                type = Router.bufferedReader.readLine().trim().toLowerCase();

                if (type.isEmpty()) {
                    System.out.println("You have to specify a type.");
                    continue;
                }

                if (options.contains(type)) {
                    break;
                }

                System.out.println("That isn't a valid type.");
            }
        }
        catch (IOException e) {
            Logger.error("IOException in Router.getVehicleType().");
            Router.stopApplication(e);
        }

        return type;
    }

    /**
     * Performs an add action.
     */
    private static void addVehicle() {
        // A list of valid types the user can enter.
        List<String> validTypes = new ArrayList<String>();
        validTypes.add("car");
        validTypes.add("truck");
        validTypes.add("trailer");

        // Get the user selected type.
        String type = Router.getVehicleType(validTypes);

        Vehicle vehicle = null;

        switch (type) {
            case "car":
                vehicle = new Car();
                break;
            case "truck":
                vehicle = new Truck();
                break;
            case "trailer":
                vehicle = new Trailer();
                break;
        }

        vehicle.add();

        // Append the new vehicle to the list.
        vehicle.appendItem();

        Logger.info("Added " + vehicle.getRegistrationNumber() + ".");

        System.out.printf("Added a %s.%n", type);
    }

    /**
     * Performs an edit action.
     */
    private static void editVehicle() {
        // Get the registration number from the user.
        String registrationNumber = Router.getRegistrationNumber();
        // Get the vehicle with the registration number.
        Vehicle vehicle = VehicleUtils.getVehcile(registrationNumber);

        // Abort if vehicle is null.
        if (vehicle == null) {
            System.out.println("Can't find a vehicle with that registration "
                    + "number");
            return;
        }

        vehicle.edit();

        // Append the vehicle to the list. As it is a hashmap, an append will
        // overwrite the old one.
        vehicle.appendItem();

        Logger.info("Edited " + registrationNumber + ".");
    }

    /**
     * Performs a delete action.
     */
    private static void deleteVehicle() {
        // Get the registration number from the user.
        String registrationNumber = Router.getRegistrationNumber();
        // Get the vehicle with the registration number.
        Vehicle vehicle = VehicleUtils.getVehcile(registrationNumber);

        // Abort if vehicle is null.
        if (vehicle == null) {
            System.out.println("Can't find a vehicle with that registration "
                    + "number");
            return;
        }

        // Ask is the user is sure about the deletion.
        try {
            while (true) {
                System.out.print("Are you sure you want to delete that "
                        + "vehcile? (y/N) :");
                String confirmation = Router.bufferedReader.readLine().trim();
                // No, do not delete, abort.
                if (confirmation.matches("[nN]")) {
                    System.out.println("Canceling the delete action.");
                    return;
                }
                // Continue to delete.
                if (confirmation.matches("[yY]")) {
                    break;
                }
            }
        }
        catch (IOException e) {
            Logger.error("IOException in Router.deleteVehicle().");
            Router.stopApplication(e);
        }

        VehicleFileManager fileManager = new VehicleFileManager(
                RentService.ITEMFILENAME,
                RentService.ITEMFILEPATH);

        // Get the list if existing vehicles.
        HashMap<String, Vehicle> list = fileManager.load();

        // Remove the vehicle from the list.
        list.remove(registrationNumber);

        // Save the new list.
        fileManager.save(list);

        Logger.info("Deleted " + registrationNumber + ".");

        System.out.printf("The vehicle [%s] were removed.%n",
                registrationNumber);
    }

    /**
     * Performs a list action.
     */
    private static void listVehicles() {
        // A list of valid types the user can enter.
        List<String> validTypes = new ArrayList<String>();
        validTypes.add("cars");
        validTypes.add("trucks");
        validTypes.add("trailers");
        validTypes.add("all");

        // Get the user selected type.
        String type = Router.getVehicleType(validTypes);

        switch (type) {
            case "cars":
                ListVehicles.listCars();
                break;
            case "trucks":
                ListVehicles.listTrucks();
                break;
            case "trailers":
                ListVehicles.listTrailers();
                break;
            case "all":
                ListVehicles.listAll();
                break;
        }

        Logger.info("Listed  " + type + ".");
    }

    /**
     * View one vehicle in a list format.
     */
    public static void viewVehicle() {
        // Get the registration number from the user.
        String registrationNumber = Router.getRegistrationNumber();
        // Get the vehicle with the registration number.
        Vehicle vehicle = VehicleUtils.getVehcile(registrationNumber);

        // Abort if vehicle is null.
        if (vehicle == null) {
            System.out.println("Can't find a vehicle with that registration "
                    + "number");
            return;
        }

        ListVehicles.printOne(vehicle);

        Logger.info("Viewd " + registrationNumber + ".");
    }

    /**
     * Rent a vehicle.
     */
    public static void rent() {
        Router.rent(true);
    }

    /**
     * Rent a vehicle.
     * 
     * @param status
     *            true when renting, false when returning.
     */
    private static void rent(boolean status) {
        // Get the registration number from the user.
        String registrationNumber = Router.getRegistrationNumber();
        // Get the vehicle with the registration number.
        Vehicle vehicle = VehicleUtils.getVehcile(registrationNumber);

        // Abort if vehicle is null.
        if (vehicle == null) {
            System.out.println("Can't find a vehicle with that registration "
                    + "number");
            return;
        }

        // Abort if the vehicle is rented.
        if (status && vehicle.isRented()) {
            System.out.println("This vehicle is already rented.");
            return;
        }

        // Abort if the vehicle isn't rented.
        if (!status && !vehicle.isRented()) {
            System.out.println("This vehicle is not rented.");
            return;
        }

        // Set status.
        vehicle.setRentStatus(status);

        // Append the vehicle to the list. Same as in the edit action.
        vehicle.appendItem();

        String label = (status) ? "rented" : "returned";
        System.out.printf("%s is now %s.%n", registrationNumber, label);

        String logLabel = (status) ? "Rented" : "Returned";
        Logger.info(logLabel + " " + registrationNumber + ".");

    }

    /**
     * Return a vehicle.
     */
    public static void rentReturn() {
        Router.rent(false);
    }

    /**
     * Stops the application in case of an error.
     */
    public static void stopApplication() {
        System.err.println("Stopping applciation!");
        Logger.info("Applciation stopped.");
        System.exit(0);
    }

    /**
     * Stops the application in case of an exception.
     * 
     * @param e
     *            an Exception to get the message from.
     */
    public static void stopApplication(Exception e) {
        System.err.println("Something went very wrong!");
        System.err.println(e.getMessage());
        System.err.println("Stopping applciation!");
        Logger.warning("Application forced to stop.");
        System.exit(0);
    }

    /**
     * Stops the application.
     */
    public static void exit() {
        System.out.println("Good bye!");
        // Try closing the bufferedReader we opened at start.
        try {
            Router.bufferedReader.close();
        }
        catch (IOException e) {
            Logger.warning("Couldn't close the buffered reader in Router.exit().");
            System.err.println("Couldn't close the buffered reader.");
        }
        finally {
            Logger.info("Application stopped.");
            System.exit(0);
        }
    }
}
