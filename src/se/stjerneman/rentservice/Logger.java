package se.stjerneman.rentservice;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.bethecoder.ascii_table.ASCIITable;
import com.bethecoder.ascii_table.ASCIITableHeader;

/**
 * Logger class that will take care of log messages.
 * 
 * @author Emil Stjerneman
 * 
 */
public class Logger extends FileManager {

    /**
     * The log file.
     */
    private static File logFile = null;

    /**
     * Log constructor.
     * 
     * @param fileName
     *            the file name.
     */
    public Logger(String fileName) {
        super(fileName, "");
        Logger.logFile = this.getFile();
    }

    /**
     * Log constructor.
     * 
     * @param fileName
     *            the file name.
     * @param filePath
     *            the file path.
     */
    public Logger(String fileName, String filePath) {
        super(fileName, filePath);
        Logger.logFile = this.getFile();
    }

    /**
     * Formats a string to insert into the log file.
     * 
     * @param severity
     *            the severity of the message.
     * @param record
     *            the message to store in the log.
     * @return a formatted message to input into the log file.
     */
    private static String formatMessage(LogLevel severity, String record) {
        long timeStamp = System.currentTimeMillis();
        return timeStamp + ";" + severity + ";" + record;
    }

    /**
     * Logs a message.
     * 
     * @param severity
     *            the severity of the message.
     * @param record
     *            the message to store in the log.
     */
    private static void log(LogLevel severity, String record) {
        BufferedWriter writer = null;
        try {
            FileWriter fileWriter = new FileWriter(Logger.logFile, true);
            writer = new BufferedWriter(fileWriter);

            writer.write(Logger.formatMessage(severity, record));
            writer.newLine();
        }
        catch (FileNotFoundException e) {
            System.out.println("Couldn't find the log file to write to.");
        }
        catch (IOException e) {
            Router.stopApplication(e);
        }
        finally {
            try {
                writer.close();
            }
            catch (IOException e) {
                System.out.println("Could not close the log file.");
            }
        }
    }

    /**
     * Logs an error message.
     * 
     * @param record
     *            the message to store in the log
     */
    public static void error(String record) {
        Logger.log(LogLevel.ERROR, record);
    }

    /**
     * Logs a warning message.
     * 
     * @param record
     *            the message to store in the log
     */
    public static void warning(String record) {
        Logger.log(LogLevel.WARNING, record);
    }

    /**
     * Logs an info message.
     * 
     * @param record
     *            the message to store in the log
     */
    public static void info(String record) {
        Logger.log(LogLevel.INFO, record);
    }

    /**
     * Logs a debug message.
     * 
     * @param record
     *            the message to store in the log
     */
    public static void debug(String record) {
        Logger.log(LogLevel.DEBUG, record);
    }

    /**
     * Prints the log.
     */
    public static void viewLog() {
        boolean listIsEmpty = false;

        ASCIITableHeader[] headers = {
                new ASCIITableHeader("Severity", ASCIITable.ALIGN_LEFT),
                new ASCIITableHeader("Time", ASCIITable.ALIGN_LEFT),
                new ASCIITableHeader("Message", ASCIITable.ALIGN_LEFT),
        };

        List<String[]> list = Logger.getLogData();

        // Add one to the list in order to print the table and show the table
        // headers.
        if (list.size() == 0) {
            listIsEmpty = true;
            list.add(new String[] {});
        }

        // Create a new string array that will contains string arrays.
        String[][] data = new String[list.size()][];
        for (int i = 0; i < list.size(); i++) {
            data[i] = list.get(i);
        }

        // Subtract one form the list, as one were added before.
        if (listIsEmpty) {
            list.remove(0);
        }

        ASCIITable.getInstance().printTable(headers, data);
    }

    /**
     * Gets data from the log file.
     * 
     * @return data from the log file.
     */
    private static List<String[]> getLogData() {
        List<String[]> list = new ArrayList<String[]>();

        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new FileReader(Logger.logFile));

            String currentLine;
            while ((currentLine = reader.readLine()) != null) {
                String[] element = currentLine.split(";");

                // Create a date object with the long value.
                Date date = new Date(Long.parseLong(element[0]));

                // Convert the date to a nice string.
                String dateFormat = "yyyy-MM-dd HH:mm:ss";
                String dateStr = new SimpleDateFormat(dateFormat).format(date);

                String[] data = new String[element.length];
                data[0] = dateStr;
                data[1] = element[1];
                data[2] = element[2];

                list.add(data);
            }

        }
        catch (FileNotFoundException e) {
            System.out.println("Couldn't find the log file to read from.");
        }
        catch (IOException e) {
            Router.stopApplication(e);
        }
        finally {
            try {
                reader.close();
            }
            catch (IOException e) {
                System.err.println("Couldn't close the reader.");
            }
        }

        return list;
    }
}
