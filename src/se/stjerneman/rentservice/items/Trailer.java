package se.stjerneman.rentservice.items;

import java.io.IOException;
import java.util.ArrayList;

import se.stjerneman.rentservice.Logger;
import se.stjerneman.rentservice.Router;

import com.bethecoder.ascii_table.ASCIITable;
import com.bethecoder.ascii_table.ASCIITableHeader;

/**
 * This class provides fields and methods for trailers.
 * 
 * @author Emil Stjerneman
 * 
 */
public class Trailer extends Vehicle {

    /**
     * Serialize unique ID.
     */
    private static final long serialVersionUID = -1330904955529713338L;

    /**
     * This vehicle's load length.
     */
    private float length;

    /**
     * This vehicle's load width.
     */
    private float width;

    /**
     * Trailer constructor.
     */
    public Trailer() {
    }

    /**
     * Gets this vehicle's loadable length.
     * 
     * @return the loadable length in meters.
     */
    public float getLength() {
        return length;
    }

    /**
     * Sets this vehicle's loadable length. Note: The length can't be less or
     * equal to zero.
     * 
     * @param length
     *            the loadable length in meters.
     * 
     * @return true when the loadable length is set, otherwise false.
     */
    public boolean setLength(float length) {
        // Test for length under or equal to zero.
        if (length <= 0) {
            return false;
        }

        this.length = length;
        return true;
    }

    /**
     * Gets this vehicle's loadable width.
     * 
     * @return the loadable width in meters.
     */
    public float getWidth() {
        return width;
    }

    /**
     * Sets this vehicle's loadable width. Note: The width can't be less or
     * equal to zero.
     * 
     * @param width
     *            the loadable width in meters.
     * 
     * @return true when the loadable width is set, otherwise false.
     */
    public boolean setWidth(float width) {
        // Test for width under or equal to zero.
        if (width <= 0) {
            return false;
        }

        this.width = width;
        return true;
    }

    /**
     * Lets the user set vehicle property values for this vehicle.
     */
    @Override
    public void add() {
        super.add();
        this.edit(true);
    }

    /**
     * Lets the user edit vehicle property values for this vehicle.
     */
    @Override
    public void edit() {
        super.edit();
        this.edit(false);
    }

    /**
     * Lets the user add/edit vehicle property values for this vehicle.
     * 
     * @param isNew
     *            true if action is add, otherwise false.
     */
    private void edit(boolean isNew) {
        String inputData = null;

        try {
            while (true) {
                String label = (isNew) ? "Load legnth:" : "Load legnth: (%s)";
                System.out.printf(label, this.getLength());

                inputData = Router.bufferedReader.readLine().trim();

                // Accept empty data in edit mode.
                if (!isNew && inputData.isEmpty()) {
                    break;
                }

                try {
                    if (this.setLength(Float.parseFloat(inputData))) {
                        break;
                    }
                }
                catch (NumberFormatException e) {
                    System.out.println("Load length must be a number.");
                }
            }

            while (true) {
                String label = (isNew) ? "Load width:" : "Load width: (%s)";
                System.out.printf(label, this.getWidth());

                inputData = Router.bufferedReader.readLine().trim();

                // Accept empty data in edit mode.
                if (!isNew && inputData.isEmpty()) {
                    break;
                }

                try {
                    if (this.setWidth(Float.parseFloat(inputData))) {
                        break;
                    }
                }
                catch (NumberFormatException e) {
                    System.out.println("Load width must be a number.");
                }
            }
        }
        catch (IOException e) {
            Logger.error("IOException in Trailer.edit().");
            Router.stopApplication(e);
        }
    }

    /**
     * Gets table headers used by the list methods.
     * 
     * @return table headers to use in the list methods.
     */
    public static ASCIITableHeader[] getTableHeaders() {
        return new ASCIITableHeader[] {
                new ASCIITableHeader("Type", ASCIITable.ALIGN_LEFT),
                new ASCIITableHeader("Reg.nr", ASCIITable.ALIGN_LEFT),
                new ASCIITableHeader("Brand", ASCIITable.ALIGN_LEFT),
                new ASCIITableHeader("Model", ASCIITable.ALIGN_LEFT),
                new ASCIITableHeader("Load area (m)", ASCIITable.ALIGN_LEFT),
                new ASCIITableHeader("Rent status", ASCIITable.ALIGN_LEFT),
        };
    }

    /**
     * Gets table data for cars.
     * 
     * @return a list of strings that will be used as data for this trailer.
     */
    @Override
    public ArrayList<String> getTableData() {
        ArrayList<String> list = super.getTableData();
        list.add(this.getLength() + " x " + this.getWidth());
        list.add((this.isRented() ? "Rented" : "Free"));
        return list;
    }

    @Override
    public String toString() {
        Object[] args = {
                this.getClass().getSimpleName(),
                this.getRegistrationNumber(),
                this.getBrand(),
                this.getModel(),
                String.format("%.1f", this.getLength()),
                String.format("%.1f", this.getWidth()),
        };
        String format = "Type: %s%n"
                + "Registration number: %s%n"
                + "Brand: %s%n"
                + "Model: %s%n"
                + "Length: %s%n"
                + "Width: %s%n";
        return String.format(format, args);
    }
}
