package se.stjerneman.rentservice.items;

/**
 * Fuels definitions that is used by vehicles.
 * 
 * @author Emil Stjerneman
 * 
 */
public enum Fuel {
    GAS, DISEL, BIOGAS, ELECTRICITY;

    /**
     * Prints the fuel as a string, and capitalize the first letter.
     */
    @Override
    public String toString() {
        return this.name().substring(0, 1)
                + this.name().substring(1).toLowerCase();
    };
}
