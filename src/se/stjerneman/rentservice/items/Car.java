package se.stjerneman.rentservice.items;

import java.util.ArrayList;
import java.util.EnumSet;

import com.bethecoder.ascii_table.ASCIITable;
import com.bethecoder.ascii_table.ASCIITableHeader;

/**
 * This class provides fields and methods for cars.
 * 
 * @author Emil Stjerneman
 * 
 */
public class Car extends DrivableVehicle {

    /**
     * Serialize unique ID.
     */
    private static final long serialVersionUID = -7953426628187765634L;

    /**
     * Car constructor.
     */
    public Car() {
    }

    /**
     * A set of valid fuels that this vehicle runs on.
     * 
     * @return A set of valid fuels for this vehicle.
     * 
     * @see Fuel
     */
    @Override
    public EnumSet<Fuel> validFules() {
        return EnumSet.of(
                Fuel.GAS,
                Fuel.DISEL,
                Fuel.ELECTRICITY,
                Fuel.BIOGAS
                );
    }

    /**
     * Gets table headers used by the list methods.
     * 
     * @return table headers to use in the list methods.
     */
    public static ASCIITableHeader[] getTableHeaders() {
        return new ASCIITableHeader[] {
                new ASCIITableHeader("Type", ASCIITable.ALIGN_LEFT),
                new ASCIITableHeader("Reg.nr", ASCIITable.ALIGN_LEFT),
                new ASCIITableHeader("Brand", ASCIITable.ALIGN_LEFT),
                new ASCIITableHeader("Model", ASCIITable.ALIGN_LEFT),
                new ASCIITableHeader("Fuel", ASCIITable.ALIGN_LEFT),
                new ASCIITableHeader("Gear", ASCIITable.ALIGN_LEFT),
                new ASCIITableHeader("Rent status", ASCIITable.ALIGN_LEFT),
        };
    }

    /**
     * Gets table data for cars.
     * 
     * @return a list of strings that will be used as data for this car.
     */
    @Override
    public ArrayList<String> getTableData() {
        ArrayList<String> list = super.getTableData();
        list.add((this.isRented() ? "Rented" : "Free"));
        return list;
    }

    @Override
    public String toString() {
        Object[] args = {
                this.getClass().getSimpleName(),
                this.getRegistrationNumber(),
                this.getBrand(),
                this.getModel(),
                this.getFuel().toString(),
                this.getGear().toString(),
        };
        String format = "Type: %s%n"
                + "Registration number: %s%n"
                + "Brand: %s%n"
                + "Model: %s%n"
                + "Fuel: %s%n"
                + "Gear: %s%n";
        return String.format(format, args);
    }
}
