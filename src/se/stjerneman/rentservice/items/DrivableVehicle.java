package se.stjerneman.rentservice.items;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;

import se.stjerneman.rentservice.Logger;
import se.stjerneman.rentservice.Router;

/**
 * This class provides basic fields and methods for drivable vehicles.
 * 
 * @author Emil Stjerneman
 * 
 */
public abstract class DrivableVehicle extends Vehicle {

    /**
     * Serialize unique ID.
     */
    private static final long serialVersionUID = 6795862942164960818L;

    /**
     * This vehicle's type of fuel.
     */
    private Fuel fuel;

    /**
     * This vehicle's type of gear.
     */
    private Gear gear;

    /**
     * The constructor for a drivable vehicle.
     */
    public DrivableVehicle() {
    }

    /**
     * Gets this vehcile's type of fuel.
     * 
     * @return this vehcile's type of fuel.
     * 
     * @see Fuel
     */
    public Fuel getFuel() {
        return fuel;
    }

    /**
     * Sets this vehcile's type of fuel.
     * 
     * @param fuel
     *            this vehcile's type of fuel.
     * 
     * @return true when the fuel is successfully set, otherwise false.
     * 
     * @see Fuel
     */
    public boolean setFuel(Fuel fuel) {
        // Validate the fuel.
        if (!this.validFules().contains(fuel)) {
            Object[] args = {
                    this.getClass().getSimpleName(),
                    fuel.toString().toString(),
                    this.validFules().toString()
            };
            System.out.printf("The %s doesn't run on %s, only on %s.%n", args);
            return false;
        }

        this.fuel = fuel;
        return true;
    }

    /**
     * Gets this vehcile's type of gear.
     * 
     * @return this vehcile's type of gear.
     * 
     * @see Gear
     */
    public Gear getGear() {
        return gear;
    }

    /**
     * Sets this vehcile's type of gear.
     * 
     * @param gear
     *            this vehcile's type of gear.
     * 
     * @see Gear
     */
    public void setGear(Gear gear) {
        this.gear = gear;
    }

    /**
     * A set of valid fuels that this vehicle runs on.
     * 
     * @return an enum set of valid fuels for this vehicle.
     */
    public abstract EnumSet<Fuel> validFules();

    /**
     * Lets the user set vehicle property values for this vehicle.
     */
    @Override
    public void add() {
        super.add();
        this.edit(true);
    }

    /**
     * Lets the user edit vehicle property values for this vehicle.
     */
    @Override
    public void edit() {
        super.edit();
        this.edit(false);
    }

    /**
     * Lets the user add/edit vehicle property values for this vehicle.
     * 
     * @param isNew
     *            true if action is add, otherwise false.
     */
    private void edit(boolean isNew) {
        String inputData = null;

        try {
            while (true) {
                String label = (isNew) ? "Fuel %s:" : "Fuel %s: (%s)";
                System.out.printf(label, this.validFules().toString(),
                        this.getBrand());

                inputData = Router.bufferedReader.readLine().trim();

                // Accept empty data in edit mode.
                if (!isNew && inputData.isEmpty()) {
                    break;
                }

                try {
                    // Tries to get a valid Fuel from the user input.
                    // "Raw" fuels are in upper case.
                    Fuel fuel = Fuel.valueOf(inputData.toUpperCase());

                    if (this.setFuel(fuel)) {
                        break;
                    }
                }
                catch (IllegalArgumentException e) {
                    System.out.println("No such fuel.");
                }
            }

            while (true) {
                String label = (isNew) ? "Gear %s:" : "Gear %s: (%s)";
                System.out.printf(label, Arrays.asList(Gear.values()),
                        this.getGear());

                inputData = Router.bufferedReader.readLine().trim();

                // Accept empty data in edit mode.
                if (!isNew && inputData.isEmpty()) {
                    break;
                }

                try {
                    // Tries to get a valid Gear from the user input.
                    // "Raw" gears are in upper case.
                    Gear gear = Gear.valueOf(inputData.toUpperCase());

                    this.setGear(gear);
                    break;

                }
                catch (IllegalArgumentException e) {
                    System.out.println("No such gear.");
                }
            }
        }
        catch (IOException e) {
            Logger.error("IOException in DrivableVehcile.edit().");
            Router.stopApplication(e);
        }
    }

    /**
     * Gets table data for drivable vehicles.
     * 
     * @return a list of strings that will be used as headers.
     */
    @Override
    public ArrayList<String> getTableData() {
        ArrayList<String> list = super.getTableData();
        list.add(this.getFuel().toString());
        list.add(this.getGear().toString());
        return list;
    }
}
