package se.stjerneman.rentservice.items;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import se.stjerneman.rentservice.Logger;
import se.stjerneman.rentservice.RentService;
import se.stjerneman.rentservice.Router;
import se.stjerneman.rentservice.VehicleFileManager;
import se.stjerneman.rentservice.utils.VehicleUtils;

/**
 * This class provides basic fields and methods for vehicles.
 * 
 * @author Emil Stjerneman
 * 
 */
public abstract class Vehicle implements Serializable {

    /**
     * Serialize unique ID.
     */
    private static final long serialVersionUID = 6891220552659215618L;

    /**
     * This vehicle's registration number.
     */
    private String registrationNumber;

    /**
     * This vehicle's brand.
     */
    private String brand;

    /**
     * This vehicle's model.
     */
    private String model;

    /**
     * This vehicle's rent status.
     */
    private boolean rentStatus;

    /**
     * Vehicle constructor.
     */
    public Vehicle() {
    }

    /**
     * Gets this vehicle's registration number.
     * 
     * @return this vehicles's registration number.
     */
    public String getRegistrationNumber() {
        return this.registrationNumber;
    }

    /**
     * Sets this vehicle's registration number.
     * 
     * @param registrationNumber
     *            the vehicle's registration number.
     * 
     * @return true when the registration number is successfully set, otherwise
     *         false.
     */
    public boolean setRegistrationNumber(String registrationNumber) {
        // Get the vehicle with the registration number.
        Vehicle vehicle = VehicleUtils.getVehcile(registrationNumber);

        // Abort if there is a vehicle with the same registration number.
        if (vehicle != null) {
            System.out.println("The registration number already exists.");
            // TODO: Find a way to about the whole add command here.
            return false;
        }

        // Test for empty registrationNumber.
        if (registrationNumber.isEmpty()) {
            System.out.println("The registration number can't be empty.");
            return false;
        }

        // Test that the registrationNumber is in a valid format.
        if (!VehicleUtils.validateRegistrationnumber(registrationNumber
                .toUpperCase())) {
            System.out.println("Couldn't validate the registration number.");
            return false;
        }

        this.registrationNumber = registrationNumber.toUpperCase();
        return true;
    }

    /**
     * Gets this vehicle's brand.
     * 
     * @return this vehicle's brand.
     */
    public String getBrand() {
        return brand;
    }

    /**
     * Sets this vehicle's brand.
     * 
     * @param brand
     *            this vehicle's brand.
     * 
     * @return true when the brand is successfully set, otherwise false.
     */
    public boolean setBrand(String brand) {
        // Test for empty brand.
        if (brand.isEmpty()) {
            System.out.println("Brand can't be empty.");
            return false;
        }

        this.brand = brand;
        return true;
    }

    /**
     * Gets the vehicle model.
     * 
     * @return the vehicle model.
     */
    public String getModel() {
        return model;
    }

    /**
     * Sets this vehicle's model.
     * 
     * @param model
     *            this vehicle's model.
     * 
     * @return true when the model is successfully set, otherwise false.
     */
    public boolean setModel(String model) {
        // Test for empty model.
        if (model.isEmpty()) {
            System.out.println("Model can't be empty.");
            return false;
        }

        this.model = model;
        return true;
    }

    /**
     * @return true when the vehicle is rented, false if it's free.
     */
    public boolean isRented() {
        return this.rentStatus;
    }

    /**
     * Set rent status on a vehicle.
     * 
     * @param rented
     *            false is free, true is rented.
     */
    public void setRentStatus(boolean rented) {
        this.rentStatus = rented;
    }

    /**
     * Lets the user set vehicle property values for this vehicle.
     */
    public void add() {
        String type = this.getClass().getSimpleName();
        System.out.println("------------------------------------");
        System.out.println("Adding " + type.toLowerCase());
        System.out.println("------------------------------------");

        this.edit(true);
    }

    /**
     * Lets the user edit vehicle property values for this vehicle.
     */
    public void edit() {
        System.out.println("------------------------------------");
        System.out.printf("Editing [%s]%n", this.getRegistrationNumber());
        System.out.println("------------------------------------");
        System.out.println("NOTE: Just press enter to skip to next property.");

        this.edit(false);
    }

    /**
     * Lets the user add/edit vehicle property values for this vehicle.
     * 
     * @param isNew
     *            true if action is add, otherwise false.
     */
    private void edit(boolean isNew) {
        String inputData = null;

        try {
            // Registration number isn't editable.
            if (isNew) {
                while (true) {
                    System.out.print("Registration number:");

                    inputData = Router.bufferedReader.readLine().trim();

                    if (this.setRegistrationNumber(inputData)) {
                        break;
                    }
                }
            }

            while (true) {
                String label = (isNew) ? "Brand:" : "Brand: (%s)";
                System.out.printf(label, this.getBrand());

                inputData = Router.bufferedReader.readLine().trim();

                // Accept empty data in edit mode.
                if (!isNew && inputData.isEmpty()) {
                    break;
                }

                if (this.setBrand(inputData)) {
                    break;
                }
            }

            while (true) {
                String label = (isNew) ? "Model:" : "Model: (%s)";
                System.out.printf(label, this.getModel());

                inputData = Router.bufferedReader.readLine().trim();

                // Accept empty data in edit mode.
                if (!isNew && inputData.isEmpty()) {
                    break;
                }

                if (this.setModel(inputData)) {
                    break;
                }
            }
        }
        catch (IOException e) {
            Logger.error("IOException in Vehicle.edit().");
            Router.stopApplication(e);
        }
    }

    /**
     * Saves this instance to a file.
     */
    public void appendItem() {
        VehicleFileManager fileManager = new VehicleFileManager(
                RentService.ITEMFILENAME,
                RentService.ITEMFILEPATH);

        // Get existing vehicles.
        HashMap<String, Vehicle> vehicleList = fileManager.load();

        if (vehicleList == null) {
            vehicleList = new HashMap<String, Vehicle>();
        }

        vehicleList.put(this.getRegistrationNumber(), this);

        // Save the new list.
        fileManager.save(vehicleList);
    }

    /**
     * Gets table data used by the list methods.
     * 
     * @return table data to use in the list methods.
     */
    public ArrayList<String> getTableData() {
        ArrayList<String> list = new ArrayList<>(4);
        list.add(this.getClass().getSimpleName());
        list.add(this.getRegistrationNumber());
        list.add(this.getBrand());
        list.add(this.getModel());
        return list;
    }
}
