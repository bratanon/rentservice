package se.stjerneman.rentservice.items;

import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;

import se.stjerneman.rentservice.Logger;
import se.stjerneman.rentservice.Router;

import com.bethecoder.ascii_table.ASCIITable;
import com.bethecoder.ascii_table.ASCIITableHeader;

/**
 * This class provides fields and methods for trucks.
 * 
 * @author Emil Stjerneman
 * 
 */
public class Truck extends DrivableVehicle {

    /**
     * Serialize unique ID.
     */
    private static final long serialVersionUID = 5127846726816118098L;

    /**
     * This vehicle's load volume in cubic meter (m<sup>3</sup>).
     */
    private float loadVolume;

    /**
     * Truck constructor.
     */
    public Truck() {
    }

    /**
     * Gets this vehicle's loadable volume.
     * 
     * @return the loadable volume in cubic meters (m<sup>3</sup>).
     */
    public float getLoadVolume() {
        return loadVolume;
    }

    /**
     * Sets this vehicle's loadable volume. Note: The volume can't be less or
     * equal to zero.
     * 
     * @param loadVolume
     *            the loadable volume in cubic meters (m<sup>3</sup>).
     * 
     * @return true when the load volume is set, otherwise false.
     */
    public boolean setLoadVolume(float loadVolume) {
        // Test for loadVolume under or equal to zero.
        if (loadVolume <= 0) {
            return false;
        }

        this.loadVolume = loadVolume;
        return true;
    }

    /**
     * Lets the user set vehicle property values for this vehicle.
     */
    @Override
    public void add() {
        super.add();
        this.edit(true);
    }

    /**
     * Lets the user edit vehicle property values for this vehicle.
     */
    @Override
    public void edit() {
        super.edit();
        this.edit(false);
    }

    /**
     * Lets the user add/edit vehicle property values for this vehicle.
     * 
     * @param isNew
     *            true if action is add, otherwise false.
     */
    private void edit(boolean isNew) {
        String inputData = null;

        try {
            while (true) {
                String label = (isNew) ? "Load volume (m\u00B3)"
                        : "Load volume (m\u00B3): (%s)";
                System.out.printf(label, this.getLoadVolume());

                inputData = Router.bufferedReader.readLine().trim();

                // Accept empty data in edit mode.
                if (!isNew && inputData.isEmpty()) {
                    break;
                }

                try {
                    if (this.setLoadVolume(Float.parseFloat(inputData))) {
                        break;
                    }
                }
                catch (NumberFormatException e) {
                    System.out.println("Load volume must be a number.");
                }
            }
        }
        catch (IOException e) {
            Logger.error("IOException in Truck.edit().");
            Router.stopApplication(e);
        }
    }

    /**
     * A set of valid fuels that this vehicle runs on.
     * 
     * @return a set of valid fuels for this vehicle.
     * 
     * @see Fuel
     */
    @Override
    public EnumSet<Fuel> validFules() {
        return EnumSet.of(
                Fuel.GAS,
                Fuel.DISEL
                );
    }

    /**
     * Gets table headers used by the list methods.
     * 
     * @return table headers to use in the list methods.
     */
    public static ASCIITableHeader[] getTableHeaders() {
        return new ASCIITableHeader[] {
                new ASCIITableHeader("Type", ASCIITable.ALIGN_LEFT),
                new ASCIITableHeader("Reg.nr", ASCIITable.ALIGN_LEFT),
                new ASCIITableHeader("Brand", ASCIITable.ALIGN_LEFT),
                new ASCIITableHeader("Model", ASCIITable.ALIGN_LEFT),
                new ASCIITableHeader("Fuel", ASCIITable.ALIGN_LEFT),
                new ASCIITableHeader("Gear", ASCIITable.ALIGN_LEFT),
                new ASCIITableHeader("Load volume (m\u00B3)",
                        ASCIITable.ALIGN_LEFT),
                new ASCIITableHeader("Rent status", ASCIITable.ALIGN_LEFT),
        };
    }

    /**
     * Gets table data for cars.
     * 
     * @return a list of strings that will be used as data for this truck.
     */
    @Override
    public ArrayList<String> getTableData() {
        ArrayList<String> list = super.getTableData();
        list.add(String.format("%.1f m\u00B3", this.getLoadVolume()));
        list.add((this.isRented() ? "Rented" : "Free"));
        return list;
    }

    @Override
    public String toString() {
        Object[] args = {
                this.getClass().getSimpleName(),
                this.getRegistrationNumber(),
                this.getBrand(),
                this.getModel(),
                this.getFuel().toString(),
                this.getGear().toString(),
                String.format("%.1f m\u00B3", this.getLoadVolume())
        };
        String format = "Type: %s%n"
                + "Registration number: %s%n"
                + "Brand: %s%n"
                + "Model: %s%n"
                + "Fuel: %s%n"
                + "Gear: %s%n"
                + "Loadable volume: %s%n";
        return String.format(format, args);
    }
}
