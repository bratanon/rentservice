package se.stjerneman.rentservice.items;

/**
 * Gear definitions that is used by vehicles.
 * 
 * @author Emil Stjerneman
 * 
 */
public enum Gear {
    MANUAL, AUTOMATIC;

    /**
     * Prints the gear as a string, and capitalize the first letter.
     */
    @Override
    public String toString() {
        return this.name().substring(0, 1)
                + this.name().substring(1).toLowerCase();
    };
}
