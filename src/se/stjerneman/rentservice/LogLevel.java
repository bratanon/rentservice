package se.stjerneman.rentservice;

/**
 * Log levels.
 * 
 * @author Emil Stjerneman
 * 
 */
public enum LogLevel {
    ERROR, WARNING, INFO, DEBUG;

    /**
     * Prints the LogLevel as a string,
     */
    @Override
    public String toString() {
        return "[" + this.name() + "]";
    };
}
