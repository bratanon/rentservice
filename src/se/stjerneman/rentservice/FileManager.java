package se.stjerneman.rentservice;

import java.io.File;
import java.io.IOException;

/**
 * A file manager class that will handle the initializing and creation of file.
 * 
 * @author Emil Stjerneman
 * 
 */
public abstract class FileManager {

    /**
     * This file's path.
     */
    private String filePath = null;

    /**
     * This file's name.
     */
    private String fileName = null;

    /**
     * The actual file.
     */
    private File file = null;

    /**
     * FileManager constructor.
     * 
     * @param fileName
     *            the file name.
     */
    public FileManager(String fileName) {
        this(fileName, "");
    }

    /**
     * FileManager constructor.
     * 
     * @param fileName
     *            the file name.
     * @param filePath
     *            the file path.
     */
    public FileManager(String fileName, String filePath) {
        this.fileName = fileName;
        this.filePath = filePath;
        this.file = new File(this.filePath, this.fileName);

        // Create the file if it doesn't exists.
        if (!this.file.exists()) {

            // Create the path if it doesn't exists.
            if (!this.file.getParentFile().exists()) {
                this.file.getParentFile().mkdirs();
            }

            try {
                // Create a new file.
                this.file.createNewFile();
            }
            catch (IOException e) {
                System.err.println("The file could not be created.");
                Router.stopApplication(e);
            }
        }

        // Check file permissions.
        this.checkFilePermissions();
    }

    /**
     * Gets this file.
     * 
     * @return this file.
     */
    public File getFile() {
        return this.file;
    }

    /**
     * Ensures that this file is readable and writable.
     */
    public void checkFilePermissions() {
        if (!this.file.canRead()) {
            System.err.println("You don't have permissions to read from the "
                    + "file");
            Router.stopApplication();
        }

        if (!this.file.canWrite()) {
            System.err.println("You don't have permissions to write to the "
                    + "file");
            Router.stopApplication();
        }
    }

}
