package se.stjerneman.rentservice;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import se.stjerneman.rentservice.items.Car;
import se.stjerneman.rentservice.items.Trailer;
import se.stjerneman.rentservice.items.Truck;
import se.stjerneman.rentservice.items.Vehicle;

import com.bethecoder.ascii_table.ASCIITable;
import com.bethecoder.ascii_table.ASCIITableHeader;

/**
 * This class contains various methods for listing vehicles.
 * 
 * @author Emil Stjerneman
 * 
 */
public class ListVehicles {

    /**
     * Gets a list of vehicles from the disk.
     * 
     * @return a list of all vehicles.
     */
    public static Map<String, Vehicle> getList() {
        VehicleFileManager fileManager = new VehicleFileManager(
                RentService.ITEMFILENAME,
                RentService.ITEMFILEPATH);

        Map<String, Vehicle> map = new TreeMap<String, Vehicle>(
                fileManager.load());

        return map;
    }

    /**
     * Lists all vehicles.
     */
    public static void listAll() {
        ListVehicles.listCars();
        ListVehicles.listTrucks();
        ListVehicles.listTrailers();
    }

    /**
     * Lists only cars.
     */
    public static void listCars() {
        // Get the list if existing vehicles.
        Map<String, Vehicle> list = ListVehicles.getList();

        // Create a new ArrayList that will contain car data.
        ArrayList<String[]> cars = new ArrayList<String[]>();

        for (Entry<String, Vehicle> vehicle : list.entrySet()) {
            if (vehicle.getValue() instanceof Car) {
                ArrayList<String> vData = vehicle.getValue().getTableData();
                String[] data = vData.toArray(new String[vData.size()]);
                cars.add(data);
            }
        }

        ListVehicles.printList("car", cars);
    }

    /**
     * Lists only trailers.
     */
    public static void listTrailers() {
        // Get the list if existing vehicles.
        Map<String, Vehicle> list = ListVehicles.getList();

        // Create a new ArrayList that will contain trailer data.
        ArrayList<String[]> trailers = new ArrayList<String[]>();

        for (Entry<String, Vehicle> vehicle : list.entrySet()) {
            if (vehicle.getValue() instanceof Trailer) {
                ArrayList<String> vData = vehicle.getValue().getTableData();
                String[] data = vData.toArray(new String[vData.size()]);
                trailers.add(data);
            }
        }

        ListVehicles.printList("trailer", trailers);
    }

    /**
     * Lists only trucks.
     */
    public static void listTrucks() {
        // Get the list if existing vehicles.
        Map<String, Vehicle> list = ListVehicles.getList();

        // Create a new ArrayList that will contain truck data.
        ArrayList<String[]> trucks = new ArrayList<String[]>();

        for (Entry<String, Vehicle> vehicle : list.entrySet()) {
            if (vehicle.getValue() instanceof Truck) {
                ArrayList<String> vData = vehicle.getValue().getTableData();
                String[] data = vData.toArray(new String[vData.size()]);
                trucks.add(data);
            }
        }

        ListVehicles.printList("truck", trucks);
    }

    /**
     * Gets the table headers for a specified type of vehicles.
     * 
     * @param type
     *            the vehicle type to get the headers for.
     * 
     * @return table headers for a specified type of vehicles.
     */
    private static ASCIITableHeader[] getHeaders(String type) {
        ASCIITableHeader[] headers = null;

        switch (type) {
            case "car":
                headers = Car.getTableHeaders();
                break;
            case "trailer":
                headers = Trailer.getTableHeaders();
                break;
            case "truck":
                headers = Truck.getTableHeaders();
                break;
        }

        return headers;
    }

    /**
     * Prints one vehicle in a list format.
     * 
     * @param vehicle
     *            a vehicle to list.
     */
    public static void printOne(Vehicle vehicle) {
        String type = vehicle.getClass().getSimpleName().toLowerCase();

        ASCIITableHeader[] headers = ListVehicles.getHeaders(type);
        if (headers == null) {
            Router.stopApplication(new Exception("No headers found."));
        }

        List<String> vehcileData = vehicle.getTableData();

        // Create a new string array that will contains string arrays.
        String[][] data = new String[1][];
        data[0] = vehcileData.toArray(new String[vehcileData.size()]);

        // Print the table.
        ASCIITable.getInstance().printTable(headers, data);
    }

    /**
     * Prints a table with vehicles.
     * 
     * @param type
     *            a string that represent the type of vehicle to show in the
     *            table.
     * @param list
     *            a list of vehicles to show as data in the tables.
     */
    private static void printList(String type, ArrayList<String[]> list) {
        boolean listIsEmpty = false;

        ASCIITableHeader[] headers = ListVehicles.getHeaders(type);
        if (headers == null) {
            Router.stopApplication(new Exception("No headers found."));
        }

        // Add one to the list in order to print the table and show the table
        // headers.
        if (list.size() == 0) {
            listIsEmpty = true;
            list.add(new String[] {});
        }

        // Create a new string array that will contains string arrays.
        String[][] data = new String[list.size()][];
        for (int i = 0; i < list.size(); i++) {
            data[i] = list.get(i);
        }

        // Subtract one form the list, as one were added before.
        if (listIsEmpty) {
            list.remove(0);
        }

        String strType = (list.size() > 1 ? (type + "s") : type);
        String strVerb = (list.size() > 1 ? "are" : "is");

        System.out.printf("There %s %d %s.%n", strVerb, list.size(), strType);
        // Print the table.
        ASCIITable.getInstance().printTable(headers, data);
    }
}
