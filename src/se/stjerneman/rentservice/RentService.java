package se.stjerneman.rentservice;

/**
 * Rent Service is a application that manage a rent central. Default it has
 * support for cars, trails and trucks, but can easily be extended with the
 * vehicles of your choice.
 * 
 * It support full CRUD for the vehicle entities.
 * 
 * @author Emil Stjerneman
 * @version 1.1
 * 
 */
public class RentService {

    /**
     * The name of the file that contains items.
     */
    public static final String ITEMFILENAME = "items.ser";

    /**
     * The path to the file that contains items.
     */
    public static final String ITEMFILEPATH = "data";

    /**
     * Application entry point.
     * 
     * @param args
     *            arguments from the java command.
     */
    public static void main(String[] args) {

        // Initialize the logger.
        new Logger("rentservice.log", "data");

        Logger.info("Application started.");

        // Create demo data when there is no file that contains vehicles.
        new DemoData();

        // Initialize the router.
        new Router();
    }
}
