package se.stjerneman.rentservice;

import java.io.File;
import java.io.IOException;
import java.util.Random;

import se.stjerneman.rentservice.items.Car;
import se.stjerneman.rentservice.items.Fuel;
import se.stjerneman.rentservice.items.Gear;
import se.stjerneman.rentservice.items.Trailer;
import se.stjerneman.rentservice.items.Truck;

/**
 * This class will import demo data when no other data is found.
 * 
 * @author Emil Stjerneman
 * 
 */
public class DemoData {

    /**
     * Demo data constructor.
     */
    public DemoData() {
        // Create a file object.
        File file = new File(RentService.ITEMFILEPATH, RentService.ITEMFILENAME);

        // Test if the file exists.
        if (!file.exists()) {
            System.out.println("No data was found.");

            try {
                System.out.print("Do you want me to create "
                        + "demo data for you?? (Y/n) :");

                String action = Router.bufferedReader.readLine().trim();

                // Only take care of "yes" answers.
                if (action.matches("[yY]")) {
                    this.createDemoCars();
                    this.createDemoTrucks();
                    this.createDemoTrailers();
                    System.out.println("Demo data created.");
                    Logger.info("Demo data created.");
                }
            }
            catch (IOException e) {
                Logger.warning("IOException in DemoData.DemoData().");
                Router.stopApplication(e);
            }
        }
    }

    /**
     * Create demo trailers.
     */
    private void createDemoTrailers() {
        for (int i = 0; i < 10; i++) {
            Trailer trailer = new Trailer();
            trailer.setRegistrationNumber("TRA " + this.getRandomNumber());
            trailer.setBrand("Tiki");
            trailer.setModel("Tiki C265");
            trailer.setLength(2.4f);
            trailer.setWidth(1.5f);
            trailer.setRentStatus(false);
            trailer.appendItem();
        }
    }

    /**
     * Create demo trucks.
     */
    private void createDemoTrucks() {
        for (int i = 0; i < 5; i++) {
            Truck truck = new Truck();
            truck.setRegistrationNumber("TRU " + this.getRandomNumber());
            truck.setBrand("Volvo");
            truck.setModel("FM11");
            truck.setFuel(Fuel.DISEL);
            truck.setGear(Gear.AUTOMATIC);
            truck.setLoadVolume((50.3f + i));
            truck.setRentStatus(false);
            truck.appendItem();
        }
    }

    /**
     * Create demo cars.
     */
    private void createDemoCars() {
        for (int i = 0; i < 5; i++) {
            Car car = new Car();
            car.setRegistrationNumber("CAR " + this.getRandomNumber());
            car.setBrand("Volvo");
            car.setModel("XC 70");
            car.setFuel(Fuel.GAS);
            car.setGear(Gear.MANUAL);
            car.setRentStatus(false);
            car.appendItem();
        }
    }

    /**
     * Generate a random number between 100 and 999.
     * 
     * @return a random number between 100 and 999.
     */
    private int getRandomNumber() {
        Random rand = new Random();
        return rand.nextInt((999 - 100) + 1) + 100;
    }
}
